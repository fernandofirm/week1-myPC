let fs = require('fs')

fs.readFile('./homework1.json', 'utf-8' , (err , data) => {

    const jsonData = JSON.parse(data)

    let total_Increase = jsonData.filter( v => {
        return v.salary < 100000
    }).map(v => v.salary * 2).reduce((p ,c) => p + c)

    let total_notIncrease = jsonData.filter( v => v.salary > 100000).map( v => v.salary).reduce((p,c) => p + c )

    let totalSalary = total_Increase + total_notIncrease
    console.log(total_Increase)
    console.log(total_notIncrease)
    console.log(totalSalary)


})