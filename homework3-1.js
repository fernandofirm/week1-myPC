let fs = require('fs')



let readHead = () => {
  return new Promise((resolve, reject) => {
    fs.readFile('head.txt' , 'utf-8', (err , dataHead) => {
      if (err){
        reject(err)
      }else{
        resolve(dataHead);
      }
    })
  })
}

let readBody = () => {
  return new Promise((resolve, reject) => {
    fs.readFile('body.txt' , 'utf-8', (err , dataBody) => {
      if (err){
        reject(err)
      }else{
        resolve(dataBody);
      }
    })
  })
}

let readLeg = () => {
  return new Promise((resolve, reject) => {
    fs.readFile('leg.txt' , 'utf-8', (err , dataLeg) => {
      if (err){
        reject(err)
      }else{
        resolve(dataLeg);
      }
    })
  })
}

let readFeet = () => {
  return new Promise((resolve, reject) => {
    fs.readFile('feet.txt' , 'utf-8', (err , dataFeet) => {
      if (err){
        reject(err)
      }else{
        resolve(dataFeet);
      }
    })
  })
}

let writeDemo2 = (allResult) => {
  return new Promise((resolve, reject)=> {
    fs.writeFile('robot.txt', allResult, 'utf-8' , (err) =>{
      if(err)
      reject(err)
      else
      resolve('Promise Success')
    })
  })
}



