let fs = require('fs')

fs.readFile('./homework1-4.json', 'utf-8' , (err , data) => {
    
    let jsonData = JSON.parse(data)
    
    let result = []
    jsonData.forEach((v, i, a) => {
        result.push(
            {
                "name": v['name'],
                "gender": v['gender'],
                "company": v['company'],
                "email": v['email'],
                "friends": Object.keys(v.friends).map(v => Math.floor(v) + 1)
            }        
        )
        
    })
    console.log(result)
   
   


})

